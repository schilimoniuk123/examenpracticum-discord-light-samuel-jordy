package ucll.be.practicumJavaGevorderdSamuelJordy.entities;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "BERICHT", schema = "JPA")
public class Bericht {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long bericht_id; //PK relatie met kanaal_Berichten

    @Column(name = "AFZENDER")
    private long afzender; //FK relatie met gebruikers_id

    @Column(name = "BERICHT")
    private String bericht;

    @Column(name = "VERZENDDATUM")
    private LocalDateTime verzendDatum;

    @Column(name = "ONTVANGER")
    private long ontvanger;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gebruikers")
    private Gebruikers gebruikers; //FK relatie met gebruikers

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kanalen")
    private Kanalen kanalen; //FK relatie met kanalen

    public Bericht() {

    }

    public void setKanalen(Kanalen kanalen) {
        this.kanalen = kanalen;
    }

    public Bericht(Builder builder) {

    }

    public void setId(long id) {
        this.bericht_id = id;
    }

    public void setAfzender(long afzender) {
        this.afzender = afzender;
    }

    public void setBericht(String bericht) {
        this.bericht = bericht;
    }

    public void setVerzendDatum() {
        this.verzendDatum = LocalDateTime.now();
    }

    public Long getId() {
        return bericht_id;
    }

    public Bericht(long bericht_id, long afzender, String bericht) {
        this.bericht_id = bericht_id;
        this.afzender = afzender;
        this.bericht = bericht;
    }

    public long getOntvanger() {
        return ontvanger;
    }

    public void setOntvanger(long ontvanger) {
        this.ontvanger = ontvanger;
    }

    public Long getAfzender() {
        return afzender;
    }

    public String getBericht() {
        return bericht;
    }

    public LocalDateTime getVerzendDatum() {
        return verzendDatum;
    }

    public static final class Builder{
        private long id;
        private long afzender;
        private String bericht;


        public Builder() {}

        public Builder(Bericht bericht)
        {
            this.id = bericht.getId();
            this.afzender = bericht.getAfzender();
            this.bericht = bericht.getBericht();
        }

        public Builder id(long val)
        {
            id = val;
            return this;
        }
        public Builder afzender(long val)
        {
            afzender = val;
            return this;
        }
        public Builder bericht(String val)
        {
            bericht = val;
            return this;
        }

        public Bericht Build()
        {
            return new Bericht(this);
        }
    }



    @Override
    public String toString() {
        return "Student{" +
                "id=" + bericht_id +
                ", afzender='" + afzender + '\'' +
                ", bericht='" + bericht + '\'' +
                ", verzendDatum='" + verzendDatum +
                '}';
    }
}
