package ucll.be.practicumJavaGevorderdSamuelJordy.entities;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "GEBRUIKERS", schema = "JPA")
public class Gebruikers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gebruikers_id; //PK naar kanaal gebruikers, PK naar berichten afzender

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "VOORNAAM")
    private String voornaam;

    @Column
    private String achternaam;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "gebruikers")
    private List<Bericht> berichten;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kanaal_id")
    private Kanalen kanalen;


    /*@OneToMany(targetEntity = Bericht.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "AFZENDER", referencedColumnName = "id")
    private List<Bericht> berichten;*/

    public Gebruikers(Builder builder){}

    public Gebruikers(){}

    public Gebruikers(String username, String voornaam, String achternaam) {
        this.username = username;
        this.voornaam = voornaam;
        this.achternaam = achternaam;
    }

    public void setId(Long id) {
        this.gebruikers_id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Kanalen getKanalen() {
        return kanalen;
    }

    public void setKanalen(Kanalen kanalen) {
        this.kanalen = kanalen;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public void setBerichten(List<Bericht> berichten) {
        this.berichten = berichten;
    }

    public void addBerichten(Bericht bericht) {
        this.berichten.add(bericht);
    }

    public List<Bericht> getBerichten() {
        return berichten;
    }

    public Long getId() {
        return gebruikers_id;
    }

    public String getUsername() {
        return username;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public String getAchternaam() {
        return achternaam;
    }

    public static final class Builder{
        private Long id;
        private String username;
        private String voornaam;
        private String achternaam;


        //public Builder() {}

        public Builder(Gebruikers gebruikers)
        {
            this.id = gebruikers.getId();
            this.voornaam = gebruikers.getVoornaam();
            this.achternaam = gebruikers.getAchternaam();

        }

        public Builder() {

        }

        public Builder username(String val)
        {
            username = val;
            return this;
        }
        public Builder voornaam(String val)
        {
            voornaam = val;
            return this;
        }
        public Builder achternaam(String val)
        {
            achternaam = val;
            return this;
        }

        public Gebruikers build() {
            return new Gebruikers(this);
        }
    }

    @Override
    public String toString() {
        return "{" +
                "id:" + gebruikers_id +
                ", username:'" + username + '\'' +
                ", voornaam:'" + voornaam + '\'' +
                ", achternaam:'" + achternaam +
                '}';
    }
}

