package ucll.be.practicumJavaGevorderdSamuelJordy.entities;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "KANALEN", schema = "JPA")
public class Kanalen {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long kanalen_id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "TOPIC")
    private String topic;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kanalen")
    private List<Gebruikers> gebruikers;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "kanalen")
    private List<Bericht> berichten;


    /*@OneToMany(targetEntity = Bericht.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "AFZENDER", referencedColumnName = "id")
    private List<Bericht> bericht;*/

    public Kanalen(){}
    public Kanalen(Builder builder){}

    public Kanalen(String name, String topic, String a) {
        this.name = name;
        this.topic = topic;
        gebruikers = new ArrayList<>();
        berichten = new ArrayList<>();
    }
    public Long getId() {
        return kanalen_id;
    }

    public String getName() {
        return name;
    }

    public String getTopic() {
        return topic;
    }



    public void setId(Long id) {
        this.kanalen_id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public List<Gebruikers> getGebruikers() {
        return gebruikers;
    }

    public void setGebruikers(List<Gebruikers> gebruikers) {
        this.gebruikers = gebruikers;
    }

    public void addGebruikers( Gebruikers gebruikers) {
        this.gebruikers.add(gebruikers);
    }

    public void setBericht(List<Bericht> bericht) {
        this.berichten = bericht;
    }

    public void addBericht(Bericht bericht) {
        this.berichten.add(bericht);
    }

    public List<Bericht> getBericht() {
        return berichten;
    }

    public static final class Builder{
        private long id;
        private String name;
        private String topic;


        public Builder() {}

        public Builder(Kanalen kanalen)
        {
            this.id = kanalen.getId();
            this.name = kanalen.getName();
            this.topic = kanalen.getTopic();
        }

        public Builder id(long val)
        {
            id = val;
            return this;
        }
        public Builder name(String val)
        {
            name = val;
            return this;
        }
        public Builder topic(String val)
        {
            topic = val;
            return this;
        }

        public Kanalen Build()
        {
            return new Kanalen(this);
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + kanalen_id +
                ", name='" + name + '\'' +
                ", topic='" + topic +
                '}';
    }
}