package ucll.be.practicumJavaGevorderdSamuelJordy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ucll.be.practicumJavaGevorderdSamuelJordy.dao.BerichtRepository;
import ucll.be.practicumJavaGevorderdSamuelJordy.dao.GebruikersRepository;
import ucll.be.practicumJavaGevorderdSamuelJordy.dao.KanalenRepository;
import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Bericht;
import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Gebruikers;
import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Kanalen;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Component
public class ControlCommandLineRunner implements CommandLineRunner {

    private final BerichtRepository berichtRepository;
    private final GebruikersRepository gebruikersRepository;
    private final KanalenRepository kanalenRepository;

    public ControlCommandLineRunner(BerichtRepository berichtRepository, GebruikersRepository gebruikersRepository, KanalenRepository kanalenRepository) {
        this.berichtRepository = berichtRepository;
        this.gebruikersRepository = gebruikersRepository;
        this.kanalenRepository = kanalenRepository;
    }


    @Override
    public void run(String... args) throws Exception {
        Gebruikers samuel = new Gebruikers("sammyeBlock", "Samuel" , "Chili");
        Gebruikers jokke = new Gebruikers("jokke123Tank", "jordy" , "Nuyts");
        Gebruikers comp = new Gebruikers("Microsoft", "Lsi" , "office");

        Kanalen kanaaltje = new Kanalen("dc kanaal","lelijke apen", "");

        gebruikersRepository.save(samuel);
        gebruikersRepository.save(jokke);
        gebruikersRepository.save(comp);
        kanalenRepository.save(kanaaltje);

    }

    //PostMapping maakt een nieuwe gebruiker aan
    public Gebruikers maakGebruikerAan(Gebruikers gebruikers)
    {
        return gebruikersRepository.save(gebruikers);
    }

    //GetMapping 1 of meerdere gebruikers met die username
    public List<Gebruikers> getGebruiker(String value, Boolean notEmptyStatus)
    {
        List<Gebruikers> gebruikers = new ArrayList<>();
        gebruikersRepository.findAll().forEach(gebruikers::add);
        if (notEmptyStatus)
        {
            gebruikers = gebruikers.stream()
                    .filter(ge -> (ge.getUsername().toLowerCase()).contains(value))
                    .collect(Collectors.toList());
        }
        return gebruikers;
    }

    //PostMapping maakt een nieuwe kanaal aan
    public Kanalen maakKanaalAan(Kanalen kanalen)
    {
        return kanalenRepository.save(kanalen);
    }

    //GetMapping 1 of meerdere kanalen met die name
    public List<Kanalen> getKanalen(String value, Boolean notEmptyStatus)
    {
        List<Kanalen> kanalen = new ArrayList<>();
        kanalenRepository.findAll().forEach(kanalen::add);
        //kanalenRepository.getAllExcludeForeign().stream().forEach(kanalen::add);
        if (notEmptyStatus)
        {
            kanalen = kanalen.stream()
                    .filter(kan -> (kan.getTopic().toLowerCase()).contains(value))
                    .collect(Collectors.toList());
        }
        return kanalen;
    }

    //PostMapping registreer een gebruiker aan een kanaal
    public void registreerGebruikerAanKanaal(Long idKanaal,Long idGebruiker)
    {
        //omdat deze methode zomaar uit het niets niet wil werken, werd het gedaan op de uigebreide manier...
        /*Kanalen kanalen1 = new Kanalen();
        kanalen1 = kanalenRepository.findOne(idGebruiker);*/

        List<Kanalen> kanalen;
        List<Gebruikers> gebruikers;
        kanalen = kanalenRepository.findById(idKanaal).stream().collect(Collectors.toList());

        for (Kanalen kanaal: kanalen) {
            if (idKanaal == kanaal.getId())
            {
                System.out.println("kanaal is gevonden");
                gebruikers = gebruikersRepository.findAll().stream().collect(Collectors.toList());
                for (Gebruikers gebruiker: gebruikers) {
                    System.out.println(gebruiker.getUsername());
                    if (gebruiker.getId() == idGebruiker)
                    {
                        System.out.println("persoon is gevonden\npersoon wordt toegevoegd");
                        gebruiker.setKanalen(kanaal);
                        kanaal.addGebruikers(gebruiker);
                        break;
                    }
                }
                break;
            }

        }

    }

    //GetMapping alle gebruikers die geregistreerd staan bij een kanaal
    public String getGeregistreerdeGebruikers(Long idKanaal)
    {
        String geregistreerdeGebruikers = "";
        List<Kanalen> kanalen = new ArrayList<>();
        kanalen = kanalenRepository.findById(idKanaal).stream().collect(Collectors.toList());
        for (Kanalen kanaal: kanalen) {
            for (Gebruikers gebruiker: kanaal.getGebruikers()) {
                geregistreerdeGebruikers += gebruiker.toString();
            }
        }
        return geregistreerdeGebruikers;
    }

    //PostMapping stuurt een bericht naar een gebruiker
    public Bericht stuurBerichtOntvanger(String idOntvanger, Bericht bericht)
    {
        bericht.setVerzendDatum();
        bericht.setOntvanger(Integer.parseInt(idOntvanger));
        return berichtRepository.save(bericht);
    }

    //PostMapping stuurt een bericht naar een kanaal
    public Bericht stuurBerichtKanaal(Long idOntvangerskanaal, Bericht bericht)
    {
        //Bericht bericht1 = new Bericht();
        bericht.setVerzendDatum();
        berichtRepository.save(bericht);
        List<Kanalen> kanalen = new ArrayList<>();

        kanalenRepository.findAll().forEach(kanalen::add);
        kanalen = kanalen.stream().filter(kan -> (kan.getId().equals(idOntvangerskanaal))).collect(Collectors.toList());
        kanalen.get(0).addBericht(bericht);
        bericht.setKanalen(kanalen.get(0));
        return bericht;
    }

    //GetMapping 1 of meerdere berichten van een gebruiker
    @JsonIgnore
    public String OntvangenBerichten(int idOntvanger)
    {
        List<Gebruikers> gebruikers = new ArrayList<>();
        List<Bericht> berichten = new ArrayList<>();
        berichtRepository.findAll().forEach(berichten::add);
        gebruikersRepository.findAll().forEach(gebruikers::add);

        berichten = berichten.stream().filter(kan -> (kan.getId().equals(idOntvanger))).collect(Collectors.toList());
        gebruikers = gebruikers.stream().filter(kan -> (kan.getId().equals(idOntvanger))).collect(Collectors.toList());
        String output = "[";
        for (Bericht bericht: berichten) {
            for (Gebruikers gebruiker: gebruikers) {
                if (gebruiker.getId() == bericht.getAfzender())
                {
                    output += "\t{"
                            + "\n\t\tid=\t"
                            + bericht.getId()
                            + ",\n\t\tafzender='\t{"
                            + "\n\t\t\tid=\t"
                            + bericht.getAfzender()
                            + ",\n\t\t\tusername='\t"
                            + gebruiker.getUsername()
                            + ",\n\t\t\tvoornaam='\t"
                            + gebruiker.getVoornaam()
                            + ",\n\t\t\tachternaam='\t"
                            + gebruiker.getAchternaam()
                            + "\n\t\t}"
                            + "\n\t\tbericht='\t"
                            + bericht.getBericht()
                            + "\n\t\tverzenddatum='\t"
                            + bericht.getVerzendDatum()
                            + "\n\t}\n";
                }
            }

        }
        output +="]";
        return output;
    }

    //GetMapping 1 of meerdere berichten van een kanaal
    public String OntvangenBerichtenkanaal(String idKanaal, String username, String bericht, boolean notEmptyStatusUsername, boolean notEmptyStatusBericht)
    {

        List<Kanalen> kanalen = new ArrayList<>();
        List<Gebruikers> gebruikers1 = new ArrayList<>();

        gebruikersRepository.findAll().forEach(gebruikers1::add);
        Kanalen kanalen1 = new Kanalen();
        kanalenRepository.findAll().forEach(kanalen::add);
        kanalen = kanalen.stream().filter(kan -> (kan.getId().equals(idKanaal))).collect(Collectors.toList());

        String output = "[";
        for (Gebruikers gebruikers: kanalen.get(0).getGebruikers()) {
            for (Bericht bericht1: gebruikers.getBerichten()) {

                if (gebruikers.getId() == bericht1.getAfzender() && ((gebruikers.getUsername() == username) && notEmptyStatusUsername ))
                {
                    output += "\t{"
                            + "\n\t\tid=\t"
                            + kanalen.get(0).getId()
                            + ",\n\t\tafzender='\t{"
                            + "\n\t\t\tid=\t"
                            + bericht1.getAfzender()
                            + ",\n\t\t\tusername='\t"
                            + gebruikers.getUsername()
                            + ",\n\t\t\tvoornaam='\t"
                            + gebruikers.getVoornaam()
                            + ",\n\t\t\tachternaam='\t"
                            + gebruikers.getAchternaam()
                            + "\n\t\t}"
                            + "\n\t\tbericht='\t"
                            + bericht1.getBericht()
                            + "\n\t\tverzenddatum='\t"
                            + bericht1.getVerzendDatum()
                            + "\n\t}\n";
                }

            }
        }
        output +="]";
        return output;
    }
}
