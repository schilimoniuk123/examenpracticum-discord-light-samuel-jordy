package ucll.be.practicumJavaGevorderdSamuelJordy.dao;

import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Gebruikers;
import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Kanalen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KanalenRepository extends JpaRepository<Kanalen, Long> {

    @Query("SELECT g.voornaam, g.achternaam, g.username FROM Kanalen JOIN Gebruikers g")
    public List<Gebruikers> getRegistreerdeGebruikers(Gebruikers gebruikers);

    @Query("SELECT g.afzender, g.bericht, g.verzendDatum FROM Kanalen k JOIN Bericht g")
    public String getKanaanBerichten();

    @Query("select k.kanalen_id, k.name, k.topic from Kanalen k")
    public List<Kanalen> getAllExcludeForeign();

}
