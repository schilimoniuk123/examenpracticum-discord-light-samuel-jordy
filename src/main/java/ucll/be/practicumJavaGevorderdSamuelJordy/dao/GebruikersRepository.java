package ucll.be.practicumJavaGevorderdSamuelJordy.dao;
import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Gebruikers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GebruikersRepository extends JpaRepository<Gebruikers, Long>{
}
