package ucll.be.practicumJavaGevorderdSamuelJordy.dao;

import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Bericht;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BerichtRepository extends JpaRepository<Bericht, Long> {


}
