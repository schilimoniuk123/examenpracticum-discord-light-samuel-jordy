package ucll.be.practicumJavaGevorderdSamuelJordy.web;
import com.fasterxml.jackson.annotation.JsonIgnore;
import ucll.be.practicumJavaGevorderdSamuelJordy.ControlCommandLineRunner;
import ucll.be.practicumJavaGevorderdSamuelJordy.dao.GebruikersRepository;
import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Bericht;
import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Gebruikers;
import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Kanalen;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ucll.be.practicumJavaGevorderdSamuelJordy.entities.Gebruikers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UrlResource{
    ControlCommandLineRunner controlCommandLineRunner;

    public UrlResource( ControlCommandLineRunner controlCommandLineRunner)
    {
        this.controlCommandLineRunner = controlCommandLineRunner;
    }

    //PostMapping maakt een nieuwe gebruiker aan
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/gebruikers")
    public Gebruikers postGebruikers(@RequestBody Gebruikers gebruiker)
    {
        return controlCommandLineRunner.maakGebruikerAan(gebruiker);
    }

    //GetMapping 1 of meerdere gebruikers met die username
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/gebruikers")
    public List<Gebruikers> getGebruikers(@RequestParam(value="username", required = false) String value)
    {
        boolean notEmptyStatus = false;
        if (value != null && ! value.isEmpty())
        {
            notEmptyStatus = true;
            value = value.toLowerCase();
        }
        return controlCommandLineRunner.getGebruiker(value, notEmptyStatus);
    }

    //PostMapping maakt een nieuwe kanaal aan
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/kanalen")
    public Kanalen postKanalen(@RequestBody Kanalen kanalen)
    {
        return controlCommandLineRunner.maakKanaalAan(kanalen);
    }

    //GetMapping 1 of meerdere kanalen met die name
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/kanalen")
    public List<Kanalen> getKanalen(@RequestParam(name="topic", required = false) String value)
    {
        boolean notEmptyStatus = false;
        if (value != null && ! value.isEmpty())
        {
            notEmptyStatus = true;
            value = value.toLowerCase();
        }
        return controlCommandLineRunner.getKanalen(value, notEmptyStatus);
    }

    //PostMapping registreer een gebruiker aan een kanaal
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/kanalen/{id}/registraties")
    public List<Gebruikers> postRegistratieKanaal(@PathVariable(value = "id") Long idKanaal, @RequestBody Long idGebruiker)
    {
        //List<Gebruikers> gebruikers = new ArrayList<>();
        Kanalen kanalen = new Kanalen();
        controlCommandLineRunner.registreerGebruikerAanKanaal(idKanaal, idGebruiker);
        return kanalen.getGebruikers();
    }

    //GetMapping alle gebruikers die geregistreerd staan bij een kanaal
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/kanalen/{id}/registraties")
    public String getRegistratieKanaal(@PathVariable(value = "id") Long idKanaal)
    {
        return controlCommandLineRunner.getGeregistreerdeGebruikers(idKanaal);
    }

    //PostMapping stuurt een bericht naar een gebruiker
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/gebruikers/{id}/berichten")
    public Bericht PostOntvangers(@PathVariable(value = "id") String idOntvanger, @RequestBody Bericht bericht)
    {
        return controlCommandLineRunner.stuurBerichtOntvanger(idOntvanger, bericht);
    }

    //PostMapping stuurt een bericht naar een kanaal
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/kanalen/{id}/berichten")
    public Bericht PostOntvangerskanaal(@PathVariable(value = "id") Long idOntvangerskanaal, @RequestBody Bericht bericht)
    {
        return controlCommandLineRunner.stuurBerichtKanaal(idOntvangerskanaal, bericht);
    }

    //GetMapping 1 of meerdere berichten van een gebruiker
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/gebruikers/{id}/berichten")

    public String getOntvangenBerichten(@PathVariable(value = "id") int idOntvanger)
    {
        return controlCommandLineRunner.OntvangenBerichten(idOntvanger);
    }

    //GetMapping 1 of meerdere berichten van een kanaal
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/kanalen/{id}/berichten")
    public String getOntvangenBerichtenKanaal(@PathVariable(value = "id") String idKanaal,
                                              @RequestParam(value="username", required = false) String username,
                                              @RequestParam(value="bericht", required = false) String bericht)
    {
        boolean notEmptyStatusUsername = false;
        if (username != null && ! username.isEmpty())
        {
            notEmptyStatusUsername = true;
        }
        boolean notEmptyStatusBericht = false;
        if (username != null && ! username.isEmpty())
        {
            notEmptyStatusBericht = true;
        }
        return controlCommandLineRunner.OntvangenBerichtenkanaal(idKanaal,
                username.toLowerCase(),
                bericht.toLowerCase(),
                notEmptyStatusUsername,
                notEmptyStatusBericht);
    }
}