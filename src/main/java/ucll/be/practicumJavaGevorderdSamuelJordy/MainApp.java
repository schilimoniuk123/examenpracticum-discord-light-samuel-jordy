package ucll.be.practicumJavaGevorderdSamuelJordy;

import com.zaxxer.hikari.util.DriverDataSource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collections;


@SpringBootApplication
public class MainApp extends SpringBootServletInitializer {

	public static void main(String[] args) {

		//link voor H2-console:
		//http://localhost:8083/h2-console/login.do?jsessionid=734a51634a2cdfbe8e3786a880c4f70a

		SpringApplication app = new SpringApplication(MainApp.class);
		app.setDefaultProperties(Collections.singletonMap("server.port", "8083")); //poort 8080 is bij sommige in gebruik, daarom een andere poort
		app.run(args);
	}
}
